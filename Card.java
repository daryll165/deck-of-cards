/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daryll.vildosola.java.pkg2.pkg02232021;

/**
 *
 * @author 2ndyrGroupA
 */
public class Card {
    public int rank;
    public int suit;
    
    
    public Card(int rank, int suit){
        this.rank = rank;
        this.suit = suit;
    }
    
    public static String rankToName(int rank){
        switch (rank){
            case 1:
                return "Ace";
            case 2:
                return "Two";
            case 3:
                return "Three";
            case 4: 
                return "Four";
            case 5:
                return "Five";
            case 6:
                return "Six";
            case 7:
                return "Seven";
            case 8:
                return "Eight";
            case 9:
                return "Nine";
            case 10:
                return "Ten";
            case 11:
                return "Jack";
            case 12:
                return "Queen";
            case 13:
                return "King";
            default:
                return null;           
        }
    }
    
    public static String suitToName(int suit){
        switch (suit){
            case 1:
                return "Diamonds";
            case 2:
                return "Clubs";
            case 3:
                return "Hearts";
            case 4:
                return "Spades";
            default:
                return null;
        }
    }
}